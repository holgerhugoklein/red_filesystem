#pragma once

#include <fileapi.h>

#include <fstream>
#include <string>

#include <typedefs.h>
#include <filesystem>

namespace RED {
	namespace file_system {

		void charToWchar(const char* str, u32 size, c16* buffer)
		{
			for (u32 i = 0; i < size; ++i)
			{
				buffer[i] = str[i];
			}
			buffer[size] = '\0';
		}

		b32 platform_doesFileExist(const char* path)
		{
			c16 buffer[1024];
			charToWchar(path, strlen(path), buffer);
			u32 fileType = GetFileAttributes(buffer);
			return fileType != INVALID_FILE_ATTRIBUTES;
		}

		void platform_readFileIntoStringSync(const char* path, char * buffer, u32 *size)
		{
			HANDLE file_handle = CreateFileA(
				path,
				GENERIC_READ,
				0,
				0,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				0);

			DWORD read_bytes = 0;

			b32 file_read_success = ReadFile(
				file_handle,
				buffer,
				*size,
				(DWORD *) size,
				0);

			CloseHandle(file_handle);

			buffer[*size] = '\0';

		}

		void platform_storeBytesIntoFileSyncOverrideExisting(const char* path, const char* bytes, u32* size, FILE_ATTRIBUTES attr)
		{

			u32 attrFlags = FILE_ATTRIBUTE_NORMAL;
			
			if (attr == FILE_ATTRIBUTES::HIDDEN)
			{
				attrFlags = FILE_ATTRIBUTE_HIDDEN;
			}

			HANDLE file_handle = CreateFileA(
				path,
				GENERIC_WRITE,
				0,
				0,
				CREATE_ALWAYS,
				attrFlags,
				0);

			b32 file_write_succeeded = WriteFile(
				file_handle,
				bytes,
				*size,
				(LPDWORD) size,
				0
				);

			CloseHandle(file_handle);
		}


		i32 platform_getFolderSubstringIndex(const char* path, u32 size)
		{
			for (i32 i = size - 1; i >= 0; --i)
			{
				if (path[i] == '/')
				{
					return i;
				}
			}
			return -1;
		}

		void platform_getFolderSubstring(const char* path, u32 path_length, char* buffer, u32 *out_length)
		{
			i32 folderSubstringIndex = platform_getFolderSubstringIndex(path, path_length);
			assert(folderSubstringIndex >= 0);
			memcpy(buffer, path, folderSubstringIndex + 1);
			buffer[folderSubstringIndex + 1] = '\0';
			*out_length = folderSubstringIndex + 1;
		}

		void platform_getFilenameSubstring(const char* path, u32 path_length, char* buffer, u32* out_length)
		{
			i32 folderSubstringIndex = platform_getFolderSubstringIndex(path, path_length);
			assert(folderSubstringIndex >= 0);
			*out_length = path_length - folderSubstringIndex - 1;
			memcpy(buffer, path + folderSubstringIndex + 1, *out_length);
			buffer[*out_length] = '\0';
		}

		void platform_getFilenameWithoutExtension(const char* filename, u32 filename_length, char* buffer, u32* out_length)
		{
			*out_length = 0;
			for (; *out_length < filename_length; ++*out_length)
			{
				if (filename[*out_length] == '.') break;
			}
			memcpy(buffer, filename, *out_length);
			buffer[*out_length] = '\0';
		}

	}
}
