#pragma once

#include <fstream>
#include <string>

#include <typedefs.h>
#include <filesystem>

namespace RED {
	namespace file_system {

		enum class FILE_ATTRIBUTES
		{
			NONE,
			HIDDEN
		};

		b32 platform_doesFileExist(const char* path);

		void platform_readFileIntoStringSync(const char* path, char* buffer, u32* size);

		void platform_storeBytesIntoFileSyncOverrideExisting(const char* path, const char* bytes, u32* size, FILE_ATTRIBUTES attr = FILE_ATTRIBUTES::NONE);

		i32 platform_getFolderSubstringIndex(const char* path, u32 size);

		void platform_getFolderSubstring(const char* path, u32 path_length, char* buffer, u32* out_length);

		void platform_getFilenameSubstring(const char* path, u32 path_length, char* buffer, u32* out_length);

		void platform_getFilenameWithoutExtension(const char* filename, u32 filename_length, char* buffer, u32* out_length);
	}
}
